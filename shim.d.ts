/**
 * .vue 文件默认导出 Vue 类型
 */
declare module '*.vue' {
  import Vue from 'vue';
  export default Vue;
}
