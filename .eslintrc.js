module.exports = {
  parser: '@typescript-eslint/parser',
  /** 启用规则 */
  extends: ['eslint:recommended', 'plugin:@typescript-eslint/recommended'],
  /** 代码运行环境 */
  env: {
    'browser': true,
    'node': true,
  },
  globals: {},

  /** 载入其他规则 */
  plugins: ['@typescript-eslint'],

  /** 解析选项 */
  parserOptions: {
    'ecmaVersion': 7,
    /** ECMA 模块 */
    'sourceType': 'module',
  },

  /** 
   * 规则
   * off: 0
   * warn: 1
   * error: 2
   */
  rules: {
    '@typescript-eslint/no-var-requires': 0,
    semi: 2,
    quotes: [2, 'single'],
    indent: [2, 2],
    'comma-spacing': [2, { before: false, after: true }],
    'comma-dangle': [2, 'always-multiline'],
  },
  overrides: [],
};
