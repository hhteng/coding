/** 判断元素是否在可视区域 */
function isVisible(target: HTMLElement):boolean {
  if (!target?.getBoundingClientRect) return false;
  const rect = target.getBoundingClientRect();
  const screenHeight = window.innerHeight;
  const screenWidth = window.innerWidth;
  return rect.left >= 0
    && rect.left < screenWidth
    && rect.top >= 0
    && rect.top < screenHeight;
}

export default isVisible;
