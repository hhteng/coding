import Vue from 'vue';

/**
 * 图片懒加载指令
 * v-lazyload="imgUrl"
 */
export const lazyLoad = {
  install(vue: typeof Vue):void {
    const sourceMap = new WeakMap<HTMLImageElement, string>();
    const observer = 
      IntersectionObserver &&
      new IntersectionObserver((changes) => {
        for (const change of changes) {
          /** 表示在可视窗口内 */
          if (change.isIntersecting) {
            const el = change.target;
            observer.unobserve(el);
            if (!(el instanceof HTMLImageElement)) return;
            el.src = sourceMap.get(el);
          }
        }
      });

    vue.directive('lazyload', {
      bind(el, binding) {
        if (!(el instanceof HTMLImageElement)) return;

        /** 不兼容环境(IE)则不进行懒加载 */
        if (!observer) {
          el.src = binding.value;
          return;
        }
        sourceMap.set(el, binding.value);
        observer.observe(el);
      },
    });
  },
};
